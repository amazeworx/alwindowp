<?php

add_action('twp_header', 'site_header');
function site_header()
{
  $custom_logo_id = get_theme_mod('custom_logo');
  $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
?>
  <header class="h-18 hidden lg:block 2xl:h-24">
    <div class="max-w-7xl mx-auto px-4 pt-3 2xl:pt-4">
      <div class="w-full flex items-end">

        <?php if (has_custom_logo()) {
          echo '<a href="' . get_bloginfo('url') . '" class="h-12 mr-4 mb-3 2xl:h-16 2xl:mb-4">';
          echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '" width="100" height="124" class="object-contain h-12 2xl:h-16">';
          echo '</a>';
        } else {
          echo '<div class="text-lg uppercase">';
          echo '<a href="' . get_bloginfo('url') . '" class="font-extrabold text-lg uppercase">';
          echo get_bloginfo('name');
          echo '</a>';
          echo '</div>';
        } ?>

        <?php
        wp_nav_menu(array(
          'menu'           => 'Desktop Menu',
          'container' => '',
          'menu_class' => 'navbar',
          'fallback_cb'    => false
        ));
        ?>

        <!-- <nav class="navbar">
          <a class="navbar-link " href="/about">About Us</a>
          <a class="navbar-link " href="/vision-mission">Vision &amp; Mission</a>
          <ul class="navbar-dropdown">
            <li><a class="navbar-link " href="/services">Services</a>
              <ul class="navbar-submenu">
                <li><a class="navbar-submenu-link " href="/services/end-point">End Point Solutions</a></li>
                <li><a class="navbar-submenu-link " href="/services/data-center-infrastructure">Data Center &amp; Infrastructure</a></li>
                <li><a class="navbar-submenu-link " href="/services/availability-data-protection">Availability &amp; Data Protection</a></li>
                <li><a class="navbar-submenu-link " href="/services/network">Network Solutions</a></li>
                <li><a class="navbar-submenu-link " href="/services/cyber-security">Cyber Security Solutions</a></li>
                <li><a class="navbar-submenu-link " href="/services/other">Other Solutions</a></li>
              </ul>
            </li>
          </ul>
          <a class="navbar-link " href="/our-partners">Our Partners</a><a class="navbar-link " href="/contact-us">Contact Us</a>
        </nav> -->
      </div>
    </div>
  </header>
  <div class="lg:hidden fixed top-0 left-0 right-0 z-30">
    <div class="mobilenavbar">
      <div class="flex px-4 py-4 w-full items-center">
        <button type="button" class="mr-4" id="open-menu">
          <svg class="menu-icon" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <path d="M3 4H21V6H3V4ZM3 11H21V13H3V11ZM3 18H21V20H3V18Z"></path>
          </svg>
        </button>
        <a href="<?php echo get_bloginfo('url'); ?>" class="flex items-end">
          <div class="inline-block">
            <svg class="navbar-logo" width="500" height="620" viewBox="0 0 500 620" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M247.822 0L500 620H343.121L247.822 385.634L154.175 620H0L247.822 0Z"></path>
            </svg>
          </div>
          <div class="navbar-title inline-block uppercase ml-1"><?php echo get_bloginfo('name'); ?></div>
        </a>
      </div>
    </div>
  </div>
  <nav id="mobilemenu" class="mobilemenu">
    <div class="p-4">
      <div class="text-right">
        <button type="button" id="close-menu">
          <svg class="fill-current text-white" width="32" height="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 10.586L16.95 5.636L18.364 7.05L13.414 12L18.364 16.95L16.95 18.364L12 13.414L7.05 18.364L5.636 16.95L10.586 12L5.636 7.05L7.05 5.636L12 10.586Z"></path>
          </svg>
        </button>
      </div>
      <?php
      wp_nav_menu(array(
        'menu'           => 'Mobile Menu',
        'container' => '',
        'menu_class' => '',
        'fallback_cb'    => false
      ));
      ?>
    </div>
  </nav>
  <div id="overlay" class="overlay"></div>
<?php
}
