// Navigation toggle
jQuery(document).ready(function ($) {

  // Mobile Navbar
  const mobilenavbar = $(".mobilenavbar");
  $(window).scroll(function () {
    const scroll = $(window).scrollTop();
    if (scroll > 50) {
      mobilenavbar.addClass("scroll");
    } else {
      mobilenavbar.removeClass("scroll");
    }
  });

  // Mobile Menu
  const mobilemenu = $('#mobilemenu');
  const overlay = $('#overlay');
  $('#open-menu, #close-menu, #overlay').click(function (e) {
    e.preventDefault();
    mobilemenu.toggleClass('open');
    overlay.toggleClass('open');
  });
  $('#menu-mobile-menu > li > a').on('click', function (e) {
    mobilemenu.toggleClass('open');
    overlay.toggleClass('open');
  });
});

jQuery(document).one('ready', function () {
  if (window.location.pathname != '/') {
    // Redirect to home if device width < 1024
    const windowWidth = jQuery(window).width();
    console.log(windowWidth);
    if (windowWidth < 1024) {
      window.location.href = "/";
    }
  }
});
