const plugin = require('tailwindcss/plugin');
const _ = require("lodash");
const twp = require('./twp.json');
const colors = require('tailwindcss/colors')

module.exports = {
  twp,
  purge: {
    content: [
      './*.php',
      './*/*.php',
      './*/*/*.php',
    ],
    options: {
      safelist: {
        standard: [/^has-/, /^align/, /^wp-/, 'font-light', 'font-normal', 'font-semibold', 'font-bold']
      }
    }
  },
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '0rem'
      },
    },
    extend: {
      colors: twp.colors,
      transitionProperty: ['hover', 'focus'],
      transform: ['hover', 'focus'],
      spacing: {
        '18': '4.5rem',
      },
      maxWidth: {
        '30': '30rem',
      },
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1400px',
      '3xl': '1536px',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      blue: colors.blue,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
    },
    fontFamily: {
      'sans': ["Nunito Sans", "ui-sans-serif", "system-ui", "-apple-system", "BlinkMacSystemFont", "Segoe UI", "Roboto", "Helvetica Neue", "Arial", "Noto Sans", "sans-serif", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"],
    },
    scale: {
      '0': '0',
      '25': '.25',
      '50': '.5',
      '75': '.75',
      '90': '.9',
      '95': '.95',
      '100': '1',
      '105': '1.05',
      '110': '1.1',
      '125': '1.25',
      '150': '1.5',
      '200': '2',
    },
  },
  plugins: [
    plugin(function ({ addUtilities, addComponents, e, prefix, config, theme }) {
      const colors = theme('colors');
      const margin = theme('margin');
      const screens = theme('screens');
      const fontSize = theme('fontSize');

      const editorColorText = _.map(config("twp.colors", {}), (value, key) => {
        return {
          [`.has-${key}-text-color`]: {
            color: value,
          },
        };
      });

      const editorColorBackground = _.map(config("twp.colors", {}), (value, key) => {
        return {
          [`.has-${key}-background-color`]: {
            backgroundColor: value,
          },
        };
      });

      const editorFontSizes = _.map(config("twp.fontSizes", {}), (value, key) => {
        return {
          [`.has-${key}-font-size`]: {
            fontSize: value[0],
            fontWeight: `${value[1] || 'normal'}`
          },
        };
      });

      const alignmentUtilities = {
        '.alignfull': {
          margin: `${margin[2] || '0.5rem'} calc(50% - 50vw)`,
          maxWidth: '100vw',
          "@apply w-screen": {}
        },
        '.alignwide': {
          "@apply -mx-16 my-2 max-w-screen-xl": {}
        },
        '.alignnone': {
          "@apply h-auto max-w-full mx-0": {}
        },
        ".aligncenter": {
          margin: `${margin[2] || '0.5rem'} auto`,
          "@apply block": {}
        },
        [`@media (min-width: ${screens.sm || '640px'})`]: {
          '.alignleft:not(.wp-block-button)': {
            marginRight: margin[2] || '0.5rem',
            "@apply float-left": {}
          },
          '.alignright:not(.wp-block-button)': {
            marginLeft: margin[2] || '0.5rem',
            "@apply float-right": {}
          },
          ".wp-block-button.alignleft a": {
            "@apply float-left mr-4": {},
          },
          ".wp-block-button.alignright a": {
            "@apply float-right ml-4": {},
          },
        },
      };

      const imageCaptions = {
        '.wp-caption': {
          "@apply inline-block": {},
          '& img': {
            marginBottom: margin[2] || '0.5rem',
            "@apply leading-none": {}
          },
        },
        '.wp-caption-text': {
          fontSize: (fontSize.sm && fontSize.sm[0]) || '0.9rem',
          color: (colors.gray && colors.gray[600]) || '#718096',
        },
      };

      addUtilities([editorColorText, editorColorBackground, alignmentUtilities, editorFontSizes, imageCaptions], {
        respectPrefix: false,
        respectImportant: false,
      });
    }),
  ]
};
