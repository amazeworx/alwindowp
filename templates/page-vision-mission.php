<?php

/**
 * Template Name: Vision Mission
 */

get_header();
$vision_text = get_field("vision_text");
$mission_text = get_field("mission_text");
$background_image = get_field("background_image");
$style = '';
if ($background_image) {
  $style = 'background-image: url(' . $background_image . ')';
}
?>

<main class="page-vision-mission">
  <div class="hero-container" style="<?php echo $style ?>">
    <div class="hero-inner">
      <div class="hero-text">
        <?php
        if ($vision_text) {
          echo '<h2 class="text-primary uppercase font-semibold">Vision</h2>';
          echo $vision_text;
        }
        ?>
        <?php
        if ($mission_text) {
          echo '<h2 class="text-primary uppercase font-semibold mt-6">Mission</h2>';
          echo $mission_text;
        }
        ?>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>