<?php

/**
 * Template Name: About
 */

get_header();
$about_us_text = get_field("about_us_text");
?>

<main class="page-about">
  <div class="hero-container">
    <div class="hero-text">
      <?php if ($about_us_text) echo $about_us_text; ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>