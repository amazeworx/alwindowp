<?php

/**
 * Template Name: Contact
 */

get_header();
$contact_us_text = get_field("contact_us_text");
?>

<main class="page-contact">
  <div class="hero-container">
    <div class="hero-inner">
      <div class="hero-text">
        <?php echo $contact_us_text; ?>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>