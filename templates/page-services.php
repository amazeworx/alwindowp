<?php

/**
 * Template Name: Services
 */

get_header();
?>

<main class="page-services">
  <div class="hero-container">

    <div class="flex px-8 pt-8 pb-6 gap-16 justify-between 2xl:pt-10 2xl:pb-2 3xl:pb-12">

      <div class="max-w-sm">
        <?php
        $headline = get_field("headline");
        $sub_headline = get_field("sub_headline");
        $intro_text = get_field("intro_text");
        if ($headline) { ?>
          <div class="border-b border-white border-opacity-50 pb-5 mb-5">
            <h1 class="text-2xl uppercase">
              <span class="font-semibold uppercase"><?php echo $headline; ?></span>
              <?php if ($sub_headline) { ?>
                <br />
                <span class="font-light uppercase"><?php echo $sub_headline; ?></span>
              <?php } ?>
            </h1>
          </div>
          <?php if ($intro_text) { ?>
            <div class="text-xs 2xl:text-sm"><?php echo $intro_text; ?></div>
          <?php } ?>
        <?php } ?>
      </div>

      <div class="max-w-sm">
        <?php
        $service_1_image = get_field("service_1_image");
        $service_1_title = get_field("service_1_title");
        $service_1_text = get_field("service_1_text");
        $service_1_url = get_field("service_1_url");
        ?>
        <a href="<?php echo $service_1_url; ?>" class="service-link mt-4">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">1</div>
          </div>
          <div class="service-img service-img-1">
            <img src="<?php echo $service_1_image['url'] ?>" width="<?php echo $service_1_image['sizes']['large-width'] ?>" height="<?php echo $service_1_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_1_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_1_text; ?></div>
        </a>
      </div>

      <div class="max-w-sm">
        <?php
        $service_2_image = get_field("service_2_image");
        $service_2_title = get_field("service_2_title");
        $service_2_text = get_field("service_2_text");
        $service_2_url = get_field("service_2_url");
        ?>
        <a href="<?php echo $service_2_url; ?>" class="service-link mt-4 xl:mt-6 2xl:mt-4">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">2</div>
          </div>
          <div class="service-img service-img-2">
            <img src="<?php echo $service_2_image['url'] ?>" width="<?php echo $service_2_image['sizes']['large-width'] ?>" height="<?php echo $service_2_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_2_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_2_text; ?></div>
        </a>
      </div>
    </div>

    <div class="flex px-8 pt-8 pb-6 gap-6 xl:pt-4 xl:gap-10 justify-between 2xl:pt-0 2xl:pb-0">
      <div class="max-w-xs">
        <?php
        $service_3_image = get_field("service_3_image");
        $service_3_title = get_field("service_3_title");
        $service_3_text = get_field("service_3_text");
        $service_3_url = get_field("service_3_url");
        ?>
        <a href="<?php echo $service_3_url; ?>" class="service-link xl:-mt-8 2xl:-mt-12">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">3</div>
          </div>
          <div class="service-img service-img-3">
            <img src="<?php echo $service_3_image['url'] ?>" width="<?php echo $service_3_image['sizes']['large-width'] ?>" height="<?php echo $service_3_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_3_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_3_text; ?></div>
        </a>
      </div>

      <div class="max-w-xs">
        <?php
        $service_4_image = get_field("service_4_image");
        $service_4_title = get_field("service_4_title");
        $service_4_text = get_field("service_4_text");
        $service_4_url = get_field("service_4_url");
        ?>
        <a href="<?php echo $service_4_url; ?>" class="service-link 2xl:-mt-2">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">4</div>
          </div>
          <div class="service-img service-img-4">
            <img src="<?php echo $service_4_image['url'] ?>" width="<?php echo $service_4_image['sizes']['large-width'] ?>" height="<?php echo $service_4_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_4_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_4_text; ?></div>
        </a>
      </div>

      <div class="max-w-xs">
        <?php
        $service_5_image = get_field("service_5_image");
        $service_5_title = get_field("service_5_title");
        $service_5_text = get_field("service_5_text");
        $service_5_url = get_field("service_5_url");
        ?>
        <a href="<?php echo $service_5_url; ?>" class="service-link xl:mt-8 2xl:mt-6">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">5</div>
          </div>
          <div class="service-img service-img-5">
            <img src="<?php echo $service_5_image['url'] ?>" width="<?php echo $service_5_image['sizes']['large-width'] ?>" height="<?php echo $service_5_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_5_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_5_text; ?></div>
        </a>
      </div>

      <div class="max-w-xs">
        <?php
        $service_6_image = get_field("service_6_image");
        $service_6_title = get_field("service_6_title");
        $service_6_text = get_field("service_6_text");
        $service_6_url = get_field("service_6_url");
        ?>
        <a href="<?php echo $service_6_url; ?>" class="service-link xl:mt-12 2xl:mt-14">
          <div class="service-num">
            <div class="service-num-border"></div>
            <div class="service-num-number">6</div>
          </div>
          <div class="service-img service-img-6">
            <img src="<?php echo $service_6_image['url'] ?>" width="<?php echo $service_6_image['sizes']['large-width'] ?>" height="<?php echo $service_6_image['sizes']['large-height'] ?>" />
          </div>
          <h2 class="service-title">
            <?php echo $service_6_title; ?>
          </h2>
          <div class="service-description"><?php echo $service_6_text; ?></div>
        </a>
      </div>

    </div>

  </div>
</main>

<?php get_footer(); ?>