<?php

/**
 * Template Name: Partners
 */

get_header();
$partners_logo = get_field("partners_logo");
?>

<main class="page-partners">
  <div class="pt-12 pb-12 px-8 max-w-7xl mx-auto">
    <div class="grid grid-cols-5 gap-y-16 gap-x-16 xl:gap-y-24 xl:gap-x-32">
      <?php
      if ($partners_logo) :
        foreach ($partners_logo as $logo) :
          echo '<div class="logo-partner">';
          echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
          echo '</div>';
        endforeach;
      endif;
      ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>