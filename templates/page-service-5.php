<?php

/**
 * Template Name: Service 5
 */

get_header();
$service_title = get_field("service_title");
$service_description = get_field("service_description");
$background_image = get_field("background_image");
$style = '';
if ($background_image) {
  $style = 'background-image: url(' . $background_image . ')';
}
$partners_logo = get_field("partners_logo");
// echo '<pre>';
// print_r($partners_logo);
// echo '</pre>';
?>

<main class="page-cyber-security">
  <div class="hero-container" style="<?php echo $style; ?>">
    <div class="hero-inner">
      <div class="hero-title">
        <h1 class="uppercase"><?php echo $service_title; ?></h1>
      </div>
      <div class="hero-text">
        <?php echo $service_description; ?>
      </div>
      <div class="hero-logos">
        <div class="hero-logos-grid">
          <?php
          if ($partners_logo) :
            echo '<div class="logo-partner"></div>';
            echo '<div class="logo-partner"></div>';
            foreach ($partners_logo as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>

          <!-- <div class="logo-partner"></div>
          <div class="logo-partner"></div>
          <div class="logo-partner"></div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/symantec.png" width="299" height="80" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/sangfor.png" width="241" height="80" />
          </div>
          <div class="logo-partner"></div>
          <div class="logo-partner"></div>
          <div class="logo-partner"></div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/forcepoint.png" width="493" height="80" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/kaspersky.png" width="332" height="80" />
          </div>
          <div class="logo-partner"></div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/sophos.png" width="222" height="40" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/radware.png" width="276" height="50" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/mcafee.png" width="288" height="80" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/fortinet.png" width="512" height="80" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/f5.png" width="46" height="42" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/ibm.png" width="80" height="32" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/cisco.png" width="79" height="42" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/checkpoint.png" width="455" height="80" />
          </div>
          <div class="logo-partner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/trendmicro.png" width="232" height="80" />
          </div> -->

        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>