<?php

/**
 * Template Name: Homepage
 */

get_header();
?>

<main class="page-homepage hidden lg:block">
  <div class="hero-container">
    <div class="hero-text">
      <?php
      $hero_text = get_field('hero_text');
      if ($hero_text) echo '<h1 class="uppercase">' . $hero_text . '</h1>';
      ?>
    </div>
  </div>
</main>

<div class="mobile-homepage lg:hidden">

  <?php
  $hero_background = get_field('hero_background');
  if ($hero_background) {
    $style = 'background-image: url(' . $hero_background . ')';
  }
  ?>
  <div id="top">
    <div class="mobile-home-hero" style="<?php echo $style ?>">
    </div>
  </div>

  <div id="about">
    <div class="flex justify-center border-b border-primary border-opacity-30">
      <div class="px-12 py-20">
        <?php
        $about_text = get_field('about_text');
        if ($about_text) echo $about_text;
        ?>
      </div>
    </div>
  </div>

  <div id="vision-mission">
    <div class="flex justify-center">
      <div class="max-w-md px-12 py-20">
        <?php
        $vision_text = get_field('vision_text');
        if ($vision_text) {
          echo '<h2 class="text-xl text-primary font-semibold uppercase mb-2">Vision</h2>';
          echo '<p class="text-lg">' . $vision_text . '</p>';
        }
        ?>
        <?php
        $mission_text = get_field('mission_text');
        if ($mission_text) {
          echo '<h2 class="text-xl text-primary font-semibold uppercase mb-2 mt-20">Mission</h2>';
          echo '<p class="text-lg">' . $mission_text . '</p>';
        }
        ?>
      </div>
    </div>
  </div>

  <?php
  $services_intro_headline = get_field('services_intro_headline');
  $services_intro_sub_headline = get_field('services_intro_sub_headline');
  $services_intro_text = get_field('services_intro_text');
  ?>
  <div id="services" class="flex flex-col bg-primary text-white">

    <div class="px-5 pt-10">
      <h2 class="text-xl font-bold uppercase"><?php echo $services_intro_headline; ?></h2>
      <h3 class="text-sm uppercase"><?php echo $services_intro_sub_headline; ?></h3>
      <div class="border-b border-white border-opacity-30 my-4"></div>
      <p><?php echo $services_intro_text; ?></p>
    </div>

    <div class="flex flex-col px-5 pt-10 mt-8 border-b border-white border-opacity-50">

      <?php
      $service_1 = get_field('service_1');
      ?>
      <div class="max-w-xs">
        <a href="#end-point" class="service-link pb-12">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">1</div>
          </div>
          <div class="relative z-30 mb-3 -ml-5 pt-6" style="max-width: 200px">
            <img src="<?php echo $service_1['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_1['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_1['service_text'] ?></p>
        </a>
      </div>

      <?php
      $service_2 = get_field('service_2');
      ?>
      <div class="max-w-xs">
        <a href="#data-center-infrastructure" class="service-link pb-12">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">2</div>
          </div>
          <div class="relative z-30 mb-3 -ml-5 pt-6" style="max-width: 240px; padding-left: 1px">
            <img src="<?php echo $service_2['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_2['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_2['service_text'] ?></p>
        </a>
      </div>

      <?php
      $service_3 = get_field('service_3');
      ?>
      <div class="max-w-xs">
        <a href="#availability-data-protection" class="service-link pb-12">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">3</div>
          </div>
          <div class="relative z-30 mb-3 -ml-5">
            <img src="<?php echo $service_3['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_3['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_3['service_text'] ?></p>
        </a>
      </div>

      <?php
      $service_4 = get_field('service_4');
      ?>
      <div class="max-w-xs">
        <a href="#network" class="service-link pb-16">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">4</div>
          </div>
          <div class="relative z-30 mb-3 -ml-5 pt-0" style="max-width: 360px; padding-left: 10px">
            <img src="<?php echo $service_4['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_4['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_4['service_text'] ?></p>
        </a>
      </div>

      <?php
      $service_5 = get_field('service_5');
      ?>
      <div class="max-w-xs">
        <a href="#cyber-security" class="service-link pb-12">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">5</div>
          </div>
          <div class="relative z-30 mb-3 -ml-16">
            <img src="<?php echo $service_5['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_5['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_5['service_text'] ?></p>
        </a>
      </div>

      <?php
      $service_6 = get_field('service_6');
      ?>
      <div class="max-w-xs">
        <a href="#other" class="service-link pb-12">
          <div class="absolute -top-6 -left-3 bottom-0">
            <div class="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
            <div class="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">6</div>
          </div>
          <div class="relative z-30 mb-3 ml-0 pt-0" style="max-width: 220px">
            <img src="<?php echo $service_6['service_image'] ?>" />
          </div>
          <h2 class="text-xl uppercase mb-3">
            <?php echo $service_6['service_title'] ?>
          </h2>
          <p class="text-sm"><?php echo $service_6['service_text'] ?></p>
        </a>
      </div>

    </div>

  </div>

  <div class="section-services">
    <div id="end-point" class="flex flex-col">

      <div class="px-5 pt-12 pb-5 bg-primary text-white">
        <div>
          <h1 class="uppercase text-2xl mb-4"><?php echo get_field('end_point_title') ?></h1>
        </div>
        <div class="text-center">
          <img src="<?php echo get_field('end_point_image') ?>" />
        </div>
      </div>

      <div class="px-5 py-4">
        <?php echo get_field('end_point_text') ?>
      </div>

      <div class="px-5 pt-2 pb-8">
        <div class="grid grid-cols-3 gap-x-10 gap-y-6">
          <?php
          $end_point_partners = get_field('end_point_partners');
          if ($end_point_partners) :
            foreach ($end_point_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>

    <div id="data-center-infrastructure" class="flex flex-col">

      <div class="px-5 pt-12 pb-5 bg-primary text-white">
        <h1 class="uppercase text-2xl mb-6"><?php echo get_field('data_center_title') ?></span></h1>
        <div>
          <?php echo get_field('data_center_text') ?>
        </div>
      </div>

      <div class="text-right">
        <img src="<?php echo get_field('data_center_image') ?>" />
      </div>

      <div class="px-5 pt-5 pb-10">
        <div class="grid grid-cols-3 gap-x-10 gap-y-6">
          <?php
          $data_center_partners = get_field('data_center_partners');
          if ($data_center_partners) :
            foreach ($data_center_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>

    <div id="availability-data-protection" class="flex flex-col">

      <div class="px-5 pt-12 pb-0 bg-primary text-white">
        <h1 class="uppercase text-2xl mb-6"><?php echo get_field('backup_title') ?></span></h1>
      </div>
      <div class="text-center">
        <img src="<?php echo get_field('backup_image') ?>" />
      </div>
      <div class="px-5 py-4">
        <?php echo get_field('backup_text') ?>
      </div>

      <div class="px-8 pt-4 pb-8">
        <div class="grid grid-cols-2 gap-x-10 gap-y-6">
          <?php
          $backup_partners = get_field('backup_partners');
          if ($backup_partners) :
            foreach ($backup_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>

    <div id="network" class="flex flex-col">

      <div class="px-5 pt-12 pb-0 bg-primary text-white">
        <h1 class="uppercase text-2xl mb-6"><?php echo get_field('network_title') ?></h1>
      </div>
      <div class="text-center">
        <img src="<?php echo get_field('network_image') ?>" />
      </div>
      <div class="-mt-2 px-5 pt-2 pb-5 bg-primary text-white">
        <?php echo get_field('network_service_list') ?>
      </div>
      <div class="px-5 py-4">
        <p class="mb-4"><?php echo get_field('network_text') ?></p>
      </div>

      <div class="px-8 pt-4 pb-8">
        <div class="grid grid-cols-2 gap-x-10 gap-y-6">
          <?php
          $network_partners = get_field('network_partners');
          if ($network_partners) :
            foreach ($network_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>

    <div id="cyber-security" class="flex flex-col">

      <div class="px-5 pt-12 pb-0 bg-primary text-white">
        <h1 class="uppercase text-2xl mb-6"><?php echo get_field('security_title') ?></span></h1>
      </div>
      <div class="text-center">
        <img src="<?php echo get_field('security_image') ?>" />
      </div>
      <div class="-mt-2 px-5 pt-2 pb-5 bg-primary text-white">
        <?php echo get_field('security_service_list') ?>
      </div>
      <div class="px-5 py-4">
        <p class="mb-4"><?php echo get_field('security_text') ?></p>
      </div>

      <div class="px-8 pt-4 pb-8">
        <div class="grid grid-cols-3 gap-x-10 gap-y-6">
          <?php
          $security_partners = get_field('security_partners');
          if ($security_partners) :
            foreach ($security_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>

    <div id="other" class="flex flex-col">

      <div class="px-5 pt-12 pb-0 bg-primary text-white">
        <h1 class="uppercase text-2xl mb-6"><?php echo get_field('other_title') ?></h1>
      </div>
      <div class="text-center">
        <img src="<?php echo get_field('other_image') ?>" />
      </div>
      <div class="px-5 py-4">
        <p class="mb-4"><?php echo get_field('other_text') ?>.</p>
        <?php echo get_field('other_service_list') ?>
      </div>

      <div class="px-8 pt-4 pb-8">
        <div class="grid grid-cols-3 gap-x-10 gap-y-6">
          <?php
          $other_partners = get_field('other_partners');
          if ($other_partners) :
            foreach ($other_partners as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>
  </div>

  <div id="partners">
    <div class="flex flex-col border-t border-primary border-opacity-30 mt-6">
      <div class="w-full px-5 pt-16 pb-5">
        <h2 class="text-2xl text-primary font-bold uppercase mb-6"><?php echo get_field('partners_title') ?></h2>
        <p class="text-xl text-gray-500 mb-6"><?php echo get_field('partners_text_top') ?></p>
      </div>

      <div class="px-5 pb-10 grid grid-cols-3 gap-y-10 gap-x-10">
        <?php
        $partners_logo = get_field('partners_logo');
        if ($partners_logo) :
          foreach ($partners_logo as $logo) :
            echo '<div class="logo-partner">';
            echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
            echo '</div>';
          endforeach;
        endif;
        ?>
      </div>

      <div class="w-full px-5 pt-5 pb-5">
        <p class="text-xl text-gray-500 mb-6"><?php echo get_field('partners_text_bottom') ?></p>
      </div>

    </div>
  </div>

  <div id="contact" class="bg-primary text-white">
    <div class="w-full px-5 pt-16 pb-16">
      <h2 class="text-2xl text-white font-bold uppercase mb-6"><?php echo get_field('contact_title') ?></h2>
      <?php echo get_field('contact_text') ?>
    </div>
  </div>

  <div class="px-4 py-4">
    <p class="text-center text-xs text-gray-500">Copyright &copy; <?php echo date('YYYY') ?> Alwindo Prima Solusi. <br />All Rights Reserved. </p>
  </div>

</div>

<?php get_footer(); ?>