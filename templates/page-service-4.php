<?php

/**
 * Template Name: Service 4
 */

get_header();

$service_title = get_field("service_title");
$service_description = get_field("service_description");
$background_image = get_field("background_image");
$style = '';
if ($background_image) {
  $style = 'background-image: url(' . $background_image . ')';
}
$partners_logo = get_field("partners_logo");
?>

<main class="page-network">
  <div class="hero-container" style="<?php echo $style; ?>">
    <div class="hero-inner">
      <div class="hero-title">
        <h1 class="uppercase"><?php echo $service_title; ?></h1>
      </div>
      <div class="hero-text">
        <?php echo $service_description; ?>
      </div>
      <div class="hero-logos">
        <div class="hero-logos-grid">
          <?php
          if ($partners_logo) :
            foreach ($partners_logo as $logo) :
              echo '<div class="logo-partner">';
              echo '<img src="' . esc_url($logo['sizes']['large']) . '" width="' . $logo['sizes']['large-width'] . '" height="' . $logo['sizes']['large-height'] . '" />';
              echo '</div>';
            endforeach;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>