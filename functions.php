<?php

/**
 * Load Required functions
 */
require get_template_directory() . '/inc/enqueue_scripts.php';
require get_template_directory() . '/inc/theme_setup.php';
require get_template_directory() . '/inc/nav_menu.php';
require get_template_directory() . '/inc/site-header.php';
